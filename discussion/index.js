console.log(Math.pow(7,3));
console.log(7 ** 3);

//template literals
let from = "sir Marts";

let message = "Hello Batch 145" + " love " + from;
console.log(message);

let es6Message = `Hello Batch 145 love, ${from}`
console.log(es6Message)
//destructure arrays and objects using ES6
let grades = [89, 87, 78, 96];
let fullName = ["Juan", "dela", "Cruz"]

//pre ES6 array destructuring
console.log(fullName[0]);
console.log(fullName[1]);

// ES6 array destructuring
let [firstName, middleName, lastName] = fullName;
console.log(lastName);

//object destructuring

const person = {
	givenName: "Jane",
	maidenName: "dela",
	familyName: "Cruz"
};

//before
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

//es6 destructuring

const {givenName, maidenName, familyName} = person;
console.log(person);
console.log(`Hello my name is ${givenName} ${maidenName} ${familyName}`);

const person2 = {
	name: "Jill",
	age: 29,
	address: {
		city: "Quezon City",
		street: "Nando Street"
	}
}

const {name, age, email = null} = person2;
console.log(email)
//ARROW FUNCTION

//BEFORE

function hello(argument) {
	console.log("Hello world")
};

//arrow function
const helloAgain = () => {
	console.log("Hello world")
};
hello();
helloAgain();
//implicit return statement
// const add = (x, y) => { return x + y};
const add = (x, y) => x + y;
let total = add(1,2);
console.log(total);

//before
function printFullName(firstName, middleInitial, lastName) {
	console.log(firstName + " " + middleInitial + " " + lastName);
};

printFullName("Chloe", "Detective", "Decker");

//using arrow function
const printFullName2 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
};

printFullName2("Chloe", "Detective", "Decker");

//arrow function using loops
const students = ["Joy", "Jade", "Judy"];

//before
students.forEach(function(student) {
	console.log(`${student} is a student`);
});

//using arrow function
students.forEach((student) => {
	console.log(`${student} is a student`);
})


//before
let gradesMap = grades.map(function(number) {
	return (number * number);
})
console.log(gradesMap);
//using arrow function
let gradesMap2 = grades.map((number) => {
	return 	(number * number);
})
console.log(gradesMap2);

//default function argument

const greet = (name = "User") => {
	return `Good morning, ${name}.`;
}

console.log(greet("Yk"));

//class based object blueprints

// allows creation of objects using classes as blueprints
// the constructor is a special method of a class for creating an object for that class

/* syntax
class className {
	constructor(propertyA, propertyB) {
		this.propertyA = propertyA;
		this.propertyB = propertyB;
	}
}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.year = year;
		this.name = name;
	}
}

//creating a new instance of car object
const myCar = new Car();
console.log(myCar);

// value of properties may be assigned after creation of an object
myCar.brand = "Mazda";
myCar.name = "MX-5 Miata";
myCar.year = 2023;
myCar.features = "Push start";
console.log(myCar);

// creating new instance of car with initialized values
const myNewCar = new Car("Ford", "Mustang convertible", )